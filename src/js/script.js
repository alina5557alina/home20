// 1. Додати новий абзац по кліку на кнопку:
// По кліку на кнопку <button id="btn-click">Click Me</button>, 
// створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">

const newLine = document.querySelector('#btn-click');
const sectionContent = document.querySelector('#content');
console.log(sectionContent);

newLine.addEventListener('click', (element) =>{
  const newParagraph = document.createElement('p');
  newParagraph.textContent = "New Paragraph";
  sectionContent.append(newParagraph);
})

// 2. Додати новий елемент форми із атрибутами:
// Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
// По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні 
// атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.

const newButton = document.createElement('button');
newButton.id = "btn-input-create";
newButton.style.cssText= 'background-color: red; color: #fff; padding: 10px 20px; border: none; cursor: pointer;';
sectionContent.append(newButton);

newButton.addEventListener('click', (element) => {
  const newInput = document.createElement('input');
  newInput.setAttribute('type', 'btn');
  newInput.setAttribute('placeholder', 'hellooooo');
  newInput.setAttribute('name', 'button');
  sectionContent.append(newInput);
})
